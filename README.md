## full_g2062upt_v1_gd_sh2_gq_r-user 11 RP1A.200720.011 2111301522 release-keys
- Manufacturer: ulefone
- Platform: mt6771
- Codename: Note_11P
- Brand: Ulefone
- Flavor: full_g2062upt_v1_gd_sh2_gq_r-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: 2111301522
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: 480
- Fingerprint: Ulefone/Note_11P/Note_11P:11/RP1A.200720.011/2111301522:user/release-keys
- OTA version: 
- Branch: full_g2062upt_v1_gd_sh2_gq_r-user-11-RP1A.200720.011-2111301522-release-keys
- Repo: ulefone_note_11p_dump_24358


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
